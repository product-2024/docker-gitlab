#!/bin/sh
SCRIPT_DIR=$(cd $(dirname -- $0) && pwd)
IMAGE_FILE=gitlab-ce-16.7.3-ce.0.tar.gz

mkdir /var/opt/gitlab

cd ${SCRIPT_DIR}
docker load -i ${IMAGE_FILE}
docker compose up -d
