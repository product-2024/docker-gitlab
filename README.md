# GitLabインストール手順

## 1. GitLabインストール

docker操作ユーザで以下コマンドを実行する。

```bash
[root@localhost docker-gitlab]# sh gitlab-install.sh
a1360aae5271: Loading layer [==================================================>]  80.37MB/80.37MB
2da3ae8168d2: Loading layer [==================================================>]  63.41MB/63.41MB
8135e64c26a1: Loading layer [==================================================>]  797.2kB/797.2kB
5c215cdb5c9d: Loading layer [==================================================>]  2.048kB/2.048kB
215df52958aa: Loading layer [==================================================>]  2.048kB/2.048kB
cb06163aa3c3: Loading layer [==================================================>]  2.048kB/2.048kB
94815142bc52: Loading layer [==================================================>]  19.97kB/19.97kB
c38b7f148204: Loading layer [==================================================>]  2.818GB/2.818GB
Loaded image: gitlab/gitlab-ce:16.7.3-ce.0
[+] Running 2/2
 ✔ Network docker-gitlab_default  Created                                                     0.0s
 ✔ Container gitlab               Started                                                     0.0s
```

## 2. dockerイメージロード確認

docker操作ユーザで以下コマンドを実行する。

```bash
docker images
REPOSITORY         TAG           IMAGE ID       CREATED        SIZE
gitlab/gitlab-ce   16.7.3-ce.0   53befad6a26b   11 hours ago   2.89GB
```

## 3. アクセス

```bash
http://localhost:8081/gitlab
```

### 4. GitLabコンテナ停止

docker操作ユーザで以下コマンドを実行する。

```bash
docker compose down
```

## 補足

- dockerイメージ保存

```bash
docker save gitlab/gitlab-ce:16.7.3-ce.0 | gzip > gitlab-ce-16.7.3-ce.0.tar.gz
```

- コンテナ起動時ログ確認

```bash
docker logs -f gitlab
```

- 初回ログイン時のみ`root`ユーザパスワード確認

```bash
sudo docker exec -it gitlab grep 'Password:' /etc/gitlab/initial_root_password
```

## GitLabとのSSH通信

- SSHキーペアの生成

```bash
[09:53:26 user01@localhost ~]
$ ssh-keygen -t rsa
Generating public/private rsa key pair.
Enter file in which to save the key (/home/user01/.ssh/id_rsa): 
Created directory '/home/user01/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/user01/.ssh/id_rsa.
Your public key has been saved in /home/user01/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:CKw+jVDrtlbhDJ3zOmLjUqLt8P5+j+Zxwk15ps/MLss user01@localhost.localdomain
The key's randomart image is:
+---[RSA 2048]----+
|                 |
|   .             |
|  ..o.           |
| ..o=. . .       |
|. o+ +. S o      |
|.+.o+..o +       |
|o+*...+ +        |
|o+*oo o*.=       |
| B*=o=o.E+*      |
+----[SHA256]-----+
```

- GitLabへ公開鍵登録

```bash
$ cat /home/user01/.ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDXlEU4yhDQr1RYVgVXFKh4GI4AXIII8mtnR40Rs14uh0yhBKlmeOzD7Dpf5WxlE0YkHBw2uRJd4itDSy4NMt/X8gJITkKUJxTL+qlN/iEKdaEYzxiOKm6lovOr/gNAGxglFJW8gnWNuY2WUUzT+/12wPL03NWJ+qAT2IFEX8eb5vuOxWoUcZudyDbe1xI8hCNRSZMkeRXI8AdlMt0SrIiPLgu5fFdqGHY277e2XhnpAlyfssnyjuO+KmcIsvivf90Lw0WByktyElXCBKTc7K/4h+ea4eWkNdCYyI3Zd1cKpoa92KG0tuGwMAoG17ak3abKErOp/Boda1xM9KTsnef1 user01@localhost.localdomain
```

GitLabへ以下の通り登録する。

![GitLabへ公開鍵登録](images/GitLabとのSSH通信1.png)

![GitLabへ公開鍵登録](images/GitLabとのSSH通信2.png)

![GitLabへ公開鍵登録](images/GitLabとのSSH通信3.png)

- トラブルシュート

以下エラーが発生する。

```bash
$ git clone ssh://git@localhost:2022/test-grp/test-prj.git
Cloning into 'test-prj'...
ssh_exchange_identification: Connection closed by remote host
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

ログを確認する。

```bash
docker exec -it gitlab /bin/bash
root@ff873ca4f23d:/# cat /var/log/gitlab/sshd/current
2024-01-07_11:46:57.22625 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
2024-01-07_11:46:57.22627 @         WARNING: UNPROTECTED PRIVATE KEY FILE!          @
2024-01-07_11:46:57.22640 @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
2024-01-07_11:46:57.22645 Permissions 0755 for '/etc/gitlab/ssh_host_ed25519_key' are too open.
2024-01-07_11:46:57.22659 It is required that your private key files are NOT accessible by others.
2024-01-07_11:46:57.22663 This private key will be ignored.
2024-01-07_11:46:57.22665 Unable to load host key "/etc/gitlab/ssh_host_ed25519_key": bad permissions
2024-01-07_11:46:57.22690 Unable to load host key: /etc/gitlab/ssh_host_ed25519_key
```

処置する。

```bash
root@ff873ca4f23d:/# chmod 0600 /etc/gitlab/ssh_host_*
```
